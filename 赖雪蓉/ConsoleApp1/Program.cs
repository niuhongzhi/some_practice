﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();//实例化对象
            dog.Bite();
            dog.Color();
            dog.Name();
            dog.Move();


            Fish fish = new Fish();
            fish.Move();
            fish.Name();
            fish.Color();
            fish.Bubble();
        }
    }
}

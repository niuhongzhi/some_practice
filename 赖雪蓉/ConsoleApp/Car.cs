﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Car
    {
        public string Name { set; get; }
        public string Color { set; get; }
        public int Wheel { set; get; }
        public void Print()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine("{0}的{1}有{2}个轮子，可正常行驶", Color, Name, Wheel);
            }
            else
            {
                Console.WriteLine("车子损坏需返厂");
            }
        }
             
    }
    }


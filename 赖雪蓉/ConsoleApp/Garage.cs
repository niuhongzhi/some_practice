﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Garage:Car
    {
        public string CanName { set; get; }
        public string Site { set; get; }
        public int Tel { set; get; }

        public void OPrint()
        {
            if (Wheel < 4)
            {
                Console.WriteLine("车子正在维修");
            }
            else
            {
                Console.WriteLine("");
            }
        }
    }
}

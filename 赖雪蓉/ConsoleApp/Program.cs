﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Garage garage = new Garage();
            Console.WriteLine("修车厂名：");
            garage.CanName = Console.ReadLine();

            Console.WriteLine("地址：");
            garage.Site = Console.ReadLine();

            Console.WriteLine("电话：");
            garage.Tel = int.Parse(Console.ReadLine());

            Console.WriteLine("车名：");
            garage.Name = Console.ReadLine();

            Console.WriteLine("车子颜色：");
            garage.Color = Console.ReadLine();

            Console.WriteLine("轮子数：");
            garage.Wheel = int.Parse(Console.ReadLine());


            garage.Print();

            garage.OPrint();
        }
    }
}

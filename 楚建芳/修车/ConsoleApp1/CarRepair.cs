﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class CarRepair:Car
    {
        
        public string CarRepairName { get; set; }
        public string Address { get; set; }
        public int Tel { get; set; }

        public override  void Action() { Console.WriteLine("{0}的{1}正在维修",Color,Name); }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Animal
    {
        public virtual string Name { get; set; }

        public virtual string Color { get; set; }

        public virtual void Action() { }
    }
}

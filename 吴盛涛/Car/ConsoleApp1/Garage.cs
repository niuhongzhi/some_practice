﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //修车厂具备，名字、地址、电话三个属性，具备修车的功能行为

    public class Garage:Car
    {
        //修车厂的名字
        public string Garagename { get; set; }
        
        //修车厂的地址
        public string Garageadd { get; set; }

        //修车厂的电话
        public string Garagenum { get; set; }

        //方法
        public void CarRepair()
        {
            if (CarWheel < 4)
            {
                Console.WriteLine("车子修理中");
            }
           
        }
    }
}

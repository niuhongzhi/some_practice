﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{/*描述车、修车厂两个类，车具备名字、颜色、轮子数三个属性、车具备跑得行为，
    车在跑的时候应该判断是否够四个轮子，如果够四个轮子可以跑起，否则送去修车厂维修。
    修车厂具备，名字、地址、电话三个属性，具备修车的功能行为*/
    
    public class Car
    {
        //车子名字
        public string CarName { get; set; }

        //车子轮子数
        public int CarWheel { get; set; }

        //车子颜色
        public string CarColor { get; set; }

        public void CarRun()
        {
            
            if (CarWheel >=4)
            {

           Console.WriteLine("车子跑起来了。");
            }
            else
            {
                Console.WriteLine("车子损坏，需维修");
            }
             
            

            //第一种
            //Console.WriteLine("车子跑起来了。");
        }
    }
}

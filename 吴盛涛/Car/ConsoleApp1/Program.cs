﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {Garage  car = new Garage();

            Console.WriteLine(" 名字");
            car.CarName = Console.ReadLine();

            Console.WriteLine("颜色");
            car.CarColor = Console.ReadLine();

            Console.WriteLine("轮子数");
            car.CarWheel = int.Parse(Console.ReadLine());

            //调用函数
            car.CarRepair();


            //第一种 直接主函数中调用车跑起来
            car.CarRun();


            //第二种 在主函数中做逻辑判断
            /*
            if (car.CarWheel >= 4)
            {
                car.CarRun();
            }
            else
            {
                Garage garage = new Garage();
                garage.CarRepair();
            }*/
        }
    }
}

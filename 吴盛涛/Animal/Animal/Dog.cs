﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    public class Dog:Animals
    {
        public override void Name()
        {
            Console.WriteLine("旺财");
        }

        public override void Color()
        {
            Console.WriteLine("白色");
        }

        public override void Move()
        {
            Console.WriteLine("移动");
        }

        public override void Behavior()
        {
            Console.WriteLine("咬人");
        }

        
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{

    public abstract class Animals
    {

        public abstract void Name();

        public abstract void Color();

        public abstract void Move();

        public abstract void Behavior();
    }
}

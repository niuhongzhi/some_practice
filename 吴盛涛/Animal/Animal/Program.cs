﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Program
    {
        static void Main(string[] args)
        {
            //实例化dog
            Dog dog = new Dog();

            //名字
            dog.Name();

            //颜色
            dog.Color();

            //移动
            dog.Move();

            //行为
            dog.Behavior();

            //实例化fish
            Fish fish = new Fish();

            //名字
            fish.Name();

            //颜色
            fish.Color();

            //移动
            fish.Move();

            //行为
            fish.Behavior();


            

    }
    }
}

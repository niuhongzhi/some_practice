﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    class Fish : Animals
    {
        public override void Behavior()
        {
            Console.WriteLine("会移动");
        }

        public override void Color()
        {
            Console.WriteLine("红色");
        }

        public override void Move()
        {
            Console.WriteLine("吐泡泡");
        }

        public override void Name()
        {
            Console.WriteLine("红鱼");
        }
    }
}

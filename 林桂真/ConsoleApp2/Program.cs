﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入动物：（鱼或者狗）");
            string type = Console.ReadLine();
            if (type.Equals("鱼"))
            {
                fish fish = new fish();
                Console.WriteLine("请输入鱼的名字：");
                fish.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入鱼的颜色：");
                fish.AnimalColor = Console.ReadLine();
                fish.action();
            }
            if (type.Equals("狗"))
            {
                Dog dog = new Dog();
                Console.WriteLine("请输入狗的名字：");
                dog.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入狗的颜色：");
                dog.AnimalColor = Console.ReadLine();
                dog.action();
            }
            Console.WriteLine("END");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public abstract class animal
    {
        public string AnimalName { get; set; }
        public string AnimalColor { get; set; }

        public virtual void behaver()
        {
            Console.WriteLine( "动物可以移动");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public  class Car
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public int Whell { get; set; }
        public virtual void Print()
        {
            if (Whell >= 4)
            {
                Console.WriteLine("{0}的{1}有{2}个轮子，可以正常行驶", Color, Name, Whell);
            }
            else {
                Console.WriteLine("汽车坏了，需要送去修车厂");
            }
        }
    } 
}
 
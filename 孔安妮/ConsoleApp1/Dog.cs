﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Dog : Animals
    { 
        public string Nickname { get; set; }

        public override void Color()
        {
            Console.WriteLine("狗狗有颜色");
        }

        public override void Name()
        {
            Console.WriteLine("狗狗有名字");
        }

        public override void Portable()
        {
            Console.WriteLine("狗狗会跑");
        }
    }
}

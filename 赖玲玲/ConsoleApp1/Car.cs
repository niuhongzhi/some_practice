﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Car : Garges
    {
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int CarWheel { get; set; }
        public void Run()
        {
            if (CarWheel >= 4)
            {
                Console.WriteLine("可以起跑");
            }
            else
            {
                Garges.Repair();
            }
        }
       

    }
}

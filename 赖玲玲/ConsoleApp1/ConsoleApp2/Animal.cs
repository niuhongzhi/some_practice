﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{


    public abstract class Animal
    {
        public static string DogName { get; set; }
        public static string DogColor { get; set; }
        public virtual void Mobile()
        {
            Console.WriteLine("移动");
        }
    }
}

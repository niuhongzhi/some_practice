﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Fish:Animal
    {
        public string FishName { get; set; }
        public string FishColor { get; set; }
        public void BlowBubble()
        {
            Console.WriteLine("鱼的行为");
            Console.WriteLine("吹泡泡");
            base.Mobile();
        }
    }
}

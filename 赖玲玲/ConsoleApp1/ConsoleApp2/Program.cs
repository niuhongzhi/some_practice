﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            Console.WriteLine("请输入狗的名字");
             Dog.DogName = Console.ReadLine();
            Console.WriteLine("请输入狗的颜色");
            Dog.DogColor = Console.ReadLine();
            dog.Bite();
            Fish fish= new Fish();
            Console.WriteLine("请输入鱼的名字");
            fish.FishName = Console.ReadLine();
            Console.WriteLine("请输入鱼的颜色");
            fish.FishColor = Console.ReadLine();
            fish.BlowBubble();
        }
    }
}

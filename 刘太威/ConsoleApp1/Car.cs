﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Car : Garage
    {
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int vehiclewheel { get; set; }
        public void Run()
        {
            if (vehiclewheel >= 4)
            {
                Console.WriteLine("车可以开动");
            }
            else
            {
                Garage.Repair();
                Console.WriteLine("请输入修车厂的名字：");
                GarageName = Console.ReadLine();
                Console.WriteLine("请输入修车厂的电话：");
                GaragePhone = int.Parse(Console.ReadLine());
                Console.WriteLine("请输入修车厂的地址：");
                GarageAddress = Console.ReadLine();

            }
            Console.WriteLine("成功");
        }
    }
}


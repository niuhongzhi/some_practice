﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("请输入车的名字：");
            car.Name = Console.ReadLine();
            Console.WriteLine("请输入车的颜色：");
            car.Colour = Console.ReadLine();
            Console.WriteLine("请输入车轮的个数：");
            car.Num = int .Parse (Console.ReadLine());
            Console.ReadKey();
        }
    }
}

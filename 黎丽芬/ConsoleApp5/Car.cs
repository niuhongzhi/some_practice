﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Car:Repairing
    {
        public string Name { get; set; }
        public string Colour { get; set; }
        public int Num {
            get { return this.Num; }
            set
            {
                if (value >= 4)
                {
                    Console.WriteLine("车子可以启动");
                }else
                {
                    Console.WriteLine("车子需要去修车厂维修");
                    Console.WriteLine("请输入修车厂的名字：");
                    GarageName = Console.ReadLine();
                    Console.WriteLine("请输入修车厂的地址：");
                    GarageSite = Console.ReadLine();
                    Console.WriteLine("请输入修车厂电话号码：");
                    GaragePN = int.Parse(Console.ReadLine());
                    Repairing.Maintain();
                }
            }
           
        }
    }
}

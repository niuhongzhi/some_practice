﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Dog:Animal
    {
        public string Name { get;  set; }
        public string Color { get; set; }
        public static void Bite()
        {
            Console.WriteLine("不可爱的狗会咬人");
            Animal.common();
        }
    }
}

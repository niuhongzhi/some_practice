﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Animal
    {
        public string name { get; set; }
        public string color { get; set; }
        public virtual void move()
        {
            Console.WriteLine("动物都具备移动的行为");
        }
    }
}

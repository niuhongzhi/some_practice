﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Car
{
    class Program
    {
        static void Main(string[] args)
        {
            SmallCar car = new SmallCar();
            Console.WriteLine("请输入车子的名字");
            car.name = Console.ReadLine();
            Console.WriteLine("请输入车子的颜色");
            car.Color = Console.ReadLine();
            Console.WriteLine("请输入车子的轮子数");
            car.Number = int.Parse(Console.ReadLine());
            car.Action();
        }
    }
}

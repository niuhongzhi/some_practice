﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Animal
    {
        public string Name { get; set; }
        public string Color { get; set; }
        
        public void Move()
        {
            Console.WriteLine("{0}在移动",Name);
        }
    }
}

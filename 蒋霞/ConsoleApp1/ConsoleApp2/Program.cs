﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        //描述动物、 狗、 鱼 三个类，
        //动物都具备name、 color两个属性、 动物都具备移动的行为。
        //狗特有的一个方法就是咬人。
        //鱼特有的一个行为：吹泡泡
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            dog.bite();
            dog.Move();
            Fish fish = new Fish();
            fish.Move();
            fish.bulu();
        }
    }
}

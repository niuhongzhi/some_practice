﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Animal
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public void move()
        {
            Console.WriteLine($"{Color}色的{Name}在移动！");
        }

    }
}

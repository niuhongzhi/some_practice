﻿using System;

namespace ConsoleApp1
{
    internal class Car:garage
    {
        public string Carname { get;  set; }
        public string Carcolor { get;  set; }
        public int Carwheel { get;  set; }

        internal void Run()
        {
            if (Carwheel >= 4)
            {
                Console.WriteLine("可起跑");
            }
            else 
            {
                garage.repair();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public abstract class animal
    {
        public string animalname { get; set; }

        public string animalcolor { get; set; }

        public virtual void Mobile()
        {
            Console.WriteLine("移动");
        }

    }
}

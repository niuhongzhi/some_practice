﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Text2
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            dog.bite();
            dog.name = "大白";
            dog.color = "蓝色";
            Console.WriteLine("狗叫"+dog.name+"是"+dog.color+"的");
            dog.move();

            Console.WriteLine();
            Console.WriteLine();

            Fish fish = new Fish();
            fish.blow();
            fish.name = "咕噜";
            fish.color = "黄色";
            Console.WriteLine("鱼叫"+fish.name+"是"+fish.color+"的");
            fish.move();
        }
        
    }
}

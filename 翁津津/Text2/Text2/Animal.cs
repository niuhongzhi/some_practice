﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text2
{
    public class Animal
    {
        //字段
        public string name;
        public string color;

        //属性
        public string Name { set; get; }
        public string Color { set; get; }

        //行为
        public void move()
        {
            Console.WriteLine("动物会移动");
        }
    }
}

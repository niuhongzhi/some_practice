﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text1
{
    public class Fixcar
    {
        //字段
        public string name;
        public string address;
        public string telephone;

        //属性
        public string Name { set; get; }
        public string Address { set; get; }
        public string Telephone { set; get; }

        //行为
        public void fixcar() 
        {
            Console.WriteLine("修车");
        }
    }
}

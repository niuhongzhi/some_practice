﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text1
{
    class Program
    {
        static void Main(string[] args)
        {
            Fixcar fix = new Fixcar();
            fix.name = "皇家修车厂";
            fix.address = "宝马路88号";
            fix.telephone = "110-120-119";

            Car car = new Car();
            car.name = "兰博基尼";
            car.color = "黄色";

            Console.WriteLine("请输入车子的轮子数");
            int wheel = int.Parse(Console.ReadLine());
            if (wheel> 4 || wheel< 4)
            {
                fix.fixcar();
                Console.WriteLine("送去修车厂修理");
                Console.WriteLine("修车厂："+fix.name+"地址："+fix.address+"联系电话："+fix.telephone);
            }
            else
            {
                car.run();
                Console.WriteLine("启动车子");
                Console.WriteLine("车子："+car.name+"颜色："+car.color);
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Text1
{
    public class Car : Fixcar
    {
        //字段
        //public string name;
        public string color;
        public int wheel;

        //属性
        //public string Name { set; get; }
        public string Color { set; get; }
        public int Wheel { set; get; }

        //行为
        public void run()
        {
            Console.WriteLine("跑");
        }
    }
}

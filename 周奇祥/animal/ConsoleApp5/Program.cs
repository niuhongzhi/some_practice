﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            //描述动物、 狗、 鱼 三个类，动物都具备name、 color两个属性、 
            //动物都具备移动的行为。狗特有的一个方法就是咬人。 鱼特有的一个行为：吹泡泡

            Animal animal = new Animal();
            Dog dog = new Dog();
            Fish fish = new Fish();
            Console.WriteLine("这个动物叫什么");
            animal.Name = string.Format(Console.ReadLine());
            Console.WriteLine("这个动物是什么颜色的");
            animal.Color = string.Format(Console.ReadLine());
            Console.WriteLine("这个动物是怎么动的");
            animal.Exercise = string.Format(Console.ReadLine());
            Console.WriteLine("这个动物是狗还是鱼（选狗写1，选鱼写2）");
            animal.Choose = int.Parse(Console.ReadLine());
            dog.Start();
            fish.Start();
            
            }


        }
    }


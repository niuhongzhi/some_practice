﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public class Garage
    {
        public string GarageName { get; set; }

        public string GarageSite { get; set; }

        public string GaragePhone { get; set; }

        public static void Maintain() 
        {
            Console.WriteLine("请交给维修厂处理");
        }
    }
}

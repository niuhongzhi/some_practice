﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    public class Can :Garage
    {
        public string Name { get; set; }

        public string Color { get; set; }

        public string Several { get; set; }

        public void  start()
        {
            if(Several >=4)
            {
                Console.WriteLine("你的车子可以起跑");
            }
            else
            {
                Garage.Maintain();
                Console.WriteLine("请输入某某修车厂的名字");
                GarageName = Console.ReadLine();

                Console.WriteLine("请输入某某修车厂的地址");
                GarageSite = Console.ReadLine();

                Console.WriteLine("请输入某某修车厂的电话");
                GaragePhone = Console.ReadLine();
            }
            Console.WriteLine("我会尽快送去修理");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class car:car_shop
    {
        public string carName { get; set; }

        public string carColor { get; set; }

        public int wheel { get; set; }

        public  void  Run()
        {
            if (wheel >= 4)
            {
                Console.WriteLine("可以上路");
            }
            else
            {
                repair();
            }
        }
    }
}

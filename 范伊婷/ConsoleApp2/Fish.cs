﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class Fish:Animal
    {
        public string FishName { get; set; }

        public string FishColor { get; set; }

        public void bubbles()
        {
            Console.WriteLine("鱼会吐泡泡");
            base.move();
        }
    }
}

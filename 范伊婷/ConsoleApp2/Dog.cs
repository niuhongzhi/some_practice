﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class Dog:Animal
    {
        public string DogName { get; set; }

        public string DogColor { get; set; }

        public void Bite()
        {
            Console.WriteLine("狗会咬人");
            base.move();
        }
    }
}

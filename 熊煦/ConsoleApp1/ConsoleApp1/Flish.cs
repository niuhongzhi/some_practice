﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Flish  : Zoo
    {
        public virtual void Color()
        {
            Console.WriteLine("鱼的颜色");
        }

        public virtual void Name()
        {
            Console.WriteLine("鱼的名字");
        }

        public virtual void Foot()
        {
            Console.WriteLine("鱼会游泳");
        }
    }
}

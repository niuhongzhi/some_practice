﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pratice
{
    public class Car:Repair
    {
        public string CarName { get; set; }

        public string carColor { get; set; }

        public int WheelsNumber { get; set; }

        public void Action()
        {
            if (WheelsNumber >= 4)
            {
                Console.WriteLine("车子可以跑");
            }
            else
            {
                Repair.RepairCar();
                Console.WriteLine("请输入修车厂的名字：");
                RepairName = Console.ReadLine();
                Console.WriteLine("请输入修车厂的地址：");
                RepairAddress = Console.ReadLine();
                Console.WriteLine("请输入修车厂的电话：");
                RepairPhone = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("结束");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Garage:Car
    {
        public string GarageName { get; set; }

        public string Address { get; set; }

        public int Tel { get; set; }

        public void IPrint()
        {
            if (Wheel < 4)
            {
                Console.WriteLine( "车子正在进行维修" );
            }
            else
            {
                Console.WriteLine( "车子可正常行驶" );
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Garage garage = new Garage();

            Console.WriteLine( "修车厂的名称：");
            garage.GarageName = Console.ReadLine();//将用户输入的内容（且内容无论是数值亦或是其它）转化成字符串，将字符串赋值于该属性

            Console.WriteLine( "修车厂的地址：" );
            garage.Address = Console.ReadLine();

            Console.WriteLine( "修车厂的电话：" );
            garage.Tel = int.Parse(Console.ReadLine());//将一行的数据转换成数值类型然后赋值于该属性

            Console.WriteLine( "车子的品牌名称：" );
            garage.Name = Console.ReadLine();

            Console.WriteLine( "车子的颜色：" );
            garage.Color = Console.ReadLine();

            Console.WriteLine( "车的轮子数：" );
            garage.Wheel = int.Parse(Console.ReadLine());

            garage.Print();

            garage.IPrint();
            
        }
    }
}

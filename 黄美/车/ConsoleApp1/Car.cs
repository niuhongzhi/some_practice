﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Car
    {
        public string Name { get; set; }

        public string Color { get; set; }

        public int Wheel { get; set; }

        public virtual void Print ()
        {
            if (Wheel >= 4)
            {
                Console.WriteLine( "{0}的{1}有{2}个轮子，可正常行驶",Color,Name,Wheel );
            }
            else
            {
                Console.WriteLine( "车子已损坏，需送去修车厂进行维修" );
            }
        }
    }
}

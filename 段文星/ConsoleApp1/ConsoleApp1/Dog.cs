﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Dog : Animals
    {
        public string Nickname { get; set; }

        public override void Name()
        {
            Console.WriteLine("狗的名字叫大黄");
        }
        public override void Color()
        {
            Console.WriteLine("狗是黄色的");
        }
        public override void Move()
        {
            Console.WriteLine("狗会跑");
        }
        public void Bite()
        {
            Console.WriteLine("狗会咬人");
        }
    }
}

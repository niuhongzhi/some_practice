﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            dog.Bite();
            dog.Color();
            dog.Name();
            dog.Move();

            Fish fish = new Fish();
            fish.Color();
            fish.Move();
            fish.Name();
            fish.Bubble();
;
        }
    }
}
